# Subhandler - subscription handler with alerts before auto-renewal


## Description

A personal tool to keep track of subscriptions to services. User interface via telegram bot. Current features:
- List all active subscriptions
- List total annual cost for subscriptions
- Remove a subscription
- Autorenewal alerts via telegram bot 5 days before auto-renewal date

## Visuals
Not atm

## Installation
Start venv at project root

```
virtualenv venv
```

Activate venv
```
source venv/bin/activate
```

Install dependencies
```
pip install -r dependencies.txt
```

add creds.py file with necessary credentials: api key, api secret, chat id

## Usage

run subhandler.py from project root
```
python subhandler.py
```

## Roadmap
No new features atm, feel free to propose

## Contributing
Open for contributions

## License
For open source projects, say how it is licensed.

## Project status
Development slowly progressing if/when I come up with new features

"""
A program for handling subscriptions:
    - query list of existing subscriptions
    - add warning before auto renewing
    - calculate and save a 'snapshot' of all active subs each month
    - remove subscription
    - add subscription
"""

import os
import json
import time
import sys
import logging
from datetime import datetime, timedelta, date
from telegram_bot import initialize_bot_interface, send_message
import settings

def start_logging():

    logging.basicConfig(
        filename=settings.GENERIC_LOG_PATH,
        format="%(asctime)s %(levelname)-8s %(message)s",
        level=logging.DEBUG,
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    logging.debug("LOGGING: Started!")


class Subhandler:

    def __init__(self, json_path: str):
        
        self._subs = self._load(json_path) #load the json dump to memory
        self._json_fname = json_path

    def _load(self, json_path: str):
        '''Load json file contents to memory'''
        try:
            with open(json_path) as f:
                sub_data = json.load(f)
        except FileNotFoundError:
            print("File not found..")
        except OSError:
            print("OS error")
        
        return sub_data
    
    def add_sub(self, name: str, cost: float, cost_period: int, autorenew: bool, expiration_date:str):
        '''Add a new subscription:
            - name
            - cost
            - auto-renewing
            - auto-renewing period
            - auto-renew warning, default True
            - expiration date
        '''
        success = False
        if name == "":
            print("Provide name.")
            raise Exception
        if not 0.0 < cost < 1000.0:
            print("Cost not in bounds.")
            raise Exception
        if not type(autorenew) == type(True):
            print("Auto renew must be boolean")
            raise Exception
        if cost_period < 1 or cost_period > 365:
            print("Cost period out of bounds")
            raise Exception
        new_item = {name : {'name': name,
                            'cost': cost,
                            'cost_period': cost_period,
                            'autorenew': autorenew,
                            'autorenew-warning': True,
                            'exp_data': expiration_date                            
                            }
                        }
        try:
            self._subs.update(new_item)
            self.update_json()
            success = True

        except Exception as err:
            print(err)

        return success


    def remove_sub(self, name: str):
        '''Remove a subscription with name'''
        success = False
        if name == "":
            raise ValueError
        if name in self._subs:

            try:
                del self._subs[name]
                self.update_json()
                success = True
            except Exception as err:
                print(err)
        else:
            print("Unable to remove dict, name not a dict key")
        return success

    def get_subs(self):
        '''Return all subscriptions'''
        return self._subs.keys()
    
    def get_total_monthly_cost(self):
        '''Get total cost'''
        total_monthly_cost = 0.0
        for _,val in self._subs.items():
            total_monthly_cost += ((val['cost'] / val['cost_period']) * 30.0) # calculate 30 day cost
        return total_monthly_cost

    def update_json(self):
        '''Update json file state'''        
        with open(self._json_fname, 'w') as f:
            json.dump(self._subs, f)

    def create_snapshot(self):
        '''Create a snapshot of the sub dict state as json'''
        snapshot_name = "subs_{}.json".format(datetime.now().strftime('%d-%m-%Y'))
        with open(snapshot_name, 'w') as f:
            json.dump(self._subs, f)
    
    def check_autorenewal_proximity(self, interval_days):
        '''Will return list of autorenewal subscription names that have expiration date upcoming'''
        for _,vals in self._subs.items():
            #check if autorenewal on
            if vals['autorenew'] and vals['autorenew-warning']:
                #convert into comparable datetime format, from ISO format "YYYY-MM-DD"
                try:                    
                    exp_date = date.fromisoformat(vals['exp_date'])
                except ValueError:
                    print(vals['exp_date'])
                comparison = date.today() + timedelta(days=interval_days)
                if comparison > exp_date:
                    send_message("{} autorenew on {}".format(vals['name'], vals['exp_date']))
    
    def update_field(self, name, field, value):
        '''Update sub field value'''
        success = False
        if name in self._subs:
            if field in self._subs[name].keys():
                try:
                    self._subs[name][field] = value
                    success = True
                except Exception as err:
                    print(err)
        return success       

def main():
    start_logging()
    """
    d = {'hbo': {'name': 'hbo',
                 'cost': 14.4,
                 'cost_period': 30,
                 'autorenew': True,
                 'exp_date': '14-09-2022'}
                 }
    
    with open('test.json', 'w') as f:
        json.dump(d, f)
    """
    try:
        sh = Subhandler('test.json')
        print(sh.get_subs())
        sh.remove_sub('hbo')
        print(sh.get_subs())
        #sh.add_sub('netflix', 11.90, 30, True, '06-09-2022')
        print(sh.get_subs())
    
        #initialize telegram bot
        initialize_bot_interface(sh)
        send_message("Subscription manager online!")
    
        last_autorenewal_check = time.time()
        while True:
            #if 24h passed since last check

            if (last_autorenewal_check + (3600 * 24)) < time.time():
                sh.check_autorenewal_proximity(5)
                last_autorenewal_check = time.time()
            time.sleep(1)
    except Exception as e:
        logging.error(e)
    
    except KeyboardInterrupt:
        print("Keyboard interrupted by user")
        sys.exit(0)
    
if __name__ == '__main__':
    main()

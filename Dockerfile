FROM python:3.8
WORKDIR /app
COPY dependencies.txt dependencies.txt
RUN pip3 install -r dependencies.txt
COPY . .
CMD ["python", "-m", "subhandler"]

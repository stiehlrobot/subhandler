import telegram
from telegram.ext import *
from telegram.ext import updater

import creds

coinlord = None
subhandler = None

def initialize_bot_interface(sub_handler):
    global subhandler
    global coinlord

    subhandler = sub_handler
    coinlord = telegram.Bot(creds.get_api_key('telegram'))

    updater = Updater(creds.get_api_key('telegram'), use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("add", add_sub))
    dp.add_handler(CommandHandler("remove", remove_sub))
    dp.add_handler(CommandHandler("subs", get_subs))
    dp.add_handler(CommandHandler("cost", total_cost))
    dp.add_handler(CommandHandler("update", update_field))
    
    dp.add_error_handler(error)

    updater.start_polling(2)

def send_message(msg):
    global coinlord
    try:
        coinlord.send_message(creds.get_chat_id(), msg)
        return 1
    except:
        return 0

def get_subs(update, context):
    '''Return all subscriptions'''
    global subhandler
    update.message.reply_text("{}".format(subhandler.get_subs()))

def remove_sub(update, context):
    '''Remove a subscription'''
    global subhandler
    
    msg_text = str(update.message.text).lower()
    cmd = msg_text.split(' ')[1]
    print(cmd)
    if subhandler.remove_sub(cmd):        
        update.message.reply_text("Successfully removed sub.")
    else:
        update.message.reply_text('Unrecognized sub name!')

def add_sub(update, context):
    '''Add a subscription'''
    global subhandler
    msg_text = str(update.message.text).lower()
    print(msg_text)
    data_in = msg_text.split(' ')
    if len(data_in) != 6:
        update.message.reply_text("Input data fault, check format.")
        return
    name = data_in[1]
    cost = float(data_in[2])
    cost_period = int(data_in[3])
    autorenew = bool(data_in[4])
    exp_date = data_in[5]
    if subhandler.add_sub(name, cost, cost_period, autorenew, exp_date):
        update.message.reply_text("Succesfully added new sub: {}".format(name))
    else:
        update.message.reply_text('Input data fault, check format.')

def total_cost(update, context):
    '''Get total monthly cost'''
    global subhandler
    update.message.reply_text("Total monthly cost from subscriptions: {}".format(subhandler.get_total_monthly_cost()))

def update_field(update, context):
    '''Update subscription field value'''
    global subhandler
    msg_text = str(update.message.text)
    args = msg_text.split(' ')
    if len(args) != 4:
        update.message.reply_text("Input data fault, check params.")
        return
    sub_name = args[1].lower()
    field_name = args[2].lower()
    value = args[3]
    
    if subhandler.update_field(sub_name, field_name, value):
        update.message.reply_text("Successfully updated {} to {}".format(field_name, value))
    else:
        update.message.reply_text("Input data fault, check format.")

def error(update, context):
    print(f"Update {update} cause error {context.error}")


def main():

    coinlord = initialize_bot_interface()
    coinlord.send_message("Welcome to cryptoland!")

if __name__=="__main__":
    main()
